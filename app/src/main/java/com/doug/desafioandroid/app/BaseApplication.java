package com.doug.desafioandroid.app;

import android.app.Application;

import com.doug.desafioandroid.rest.GitHubApi;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EApplication;

/**
 * Created by doug on 19/10/17.
 */

@EApplication
public class BaseApplication extends Application {

    @Bean
    GitHubApi gitHubApi;

}
