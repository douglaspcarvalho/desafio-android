package com.doug.desafioandroid.ui;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * Created by doug on 20/10/17.
 */

@EActivity
public abstract class BaseActivity extends AppCompatActivity {

    @ViewById
    ProgressBar loading;

    @ViewById
    ProgressBar bottomLoading;

    protected void showLoading(){
        loading.setVisibility(View.VISIBLE);
        notifyListener(mListener);
    }

    protected void hideLoading(){
        loading.setVisibility(View.GONE);
        notifyListener(mListener);
    }

    protected void showBottomLoading(){
        bottomLoading.setVisibility(View.VISIBLE);
    }

    protected void hideBottomLoading(){
        bottomLoading.setVisibility(View.GONE);
    }

    public boolean isLoadingShowing() {
        // return true if progress is visible
        if (loading.getVisibility() == View.VISIBLE)
            return true;
        else
            return false;

    }

    /**
     * The above Listener has the role to help with Espresso Tests using IdlingResource
     */
    private ProgressListener mListener;

    public void setProgressListener(ProgressListener progressListener) {
        mListener = progressListener;
    }

    public interface ProgressListener {
        void onProgressShown();
        void onProgressDismissed();
    }

    private void notifyListener(ProgressListener listener) {
        if (listener == null){
            return;
        }
        if (isLoadingShowing()){
            listener.onProgressShown();
        }
        else {
            listener.onProgressDismissed();
        }
    }

}
