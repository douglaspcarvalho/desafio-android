package com.doug.desafioandroid.ui.adapter;

import android.arch.paging.PagedListAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.doug.desafioandroid.rest.model.Repository;
import com.doug.desafioandroid.ui.PullRequestActivity_;
import com.doug.desafioandroid.ui.itemview.RepositoryItemView;
import com.doug.desafioandroid.ui.itemview.RepositoryItemView_;

import org.androidannotations.annotations.EBean;

/**
 * Created by doug on 19/10/17.
 */
@EBean
public class RepositoryAdapter extends PagedListAdapter<Repository, RepositoryAdapter.RepositoryItemViewHolder> {

    public RepositoryAdapter(){
        super(Repository.DIFF_CALLBACK);
    }

    @Override
    public RepositoryAdapter.RepositoryItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RepositoryItemView repositoryItemView = RepositoryItemView_.build(parent.getContext());
        return new RepositoryItemViewHolder(repositoryItemView);
    }

    @Override
    public void onBindViewHolder(RepositoryAdapter.RepositoryItemViewHolder holder, int position) {
        RepositoryItemView repositoryItemView = (RepositoryItemView) holder.itemView;
        Repository repository = getItem(position);

        repositoryItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PullRequestActivity_.intent(holder.itemView.getContext())
                        .ownerLogin(repository.getOwner().getLogin()).repositoryName(repository.getName())
                        .start();
            }
        });
        repositoryItemView.bind(repository);
    }

    static class RepositoryItemViewHolder extends RecyclerView.ViewHolder {
        public RepositoryItemViewHolder(View itemView) {
            super(itemView);
        }
    }

}
