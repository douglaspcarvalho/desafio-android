package com.doug.desafioandroid.ui.adapter;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.doug.desafioandroid.rest.model.PullRequest;
import com.doug.desafioandroid.ui.itemview.PullRequestItemView;
import com.doug.desafioandroid.ui.itemview.PullRequestItemView_;

import org.androidannotations.annotations.EBean;

import java.util.List;

/**
 * Created by doug on 19/10/17.
 */
@EBean
public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.ViewHolder> {

    private List<PullRequest> pullRequests;

    public void setPullRequests(List<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PullRequestItemView pullRequestItemView = PullRequestItemView_.build(parent.getContext());
        return new PullRequestAdapter.ViewHolder(pullRequestItemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ((PullRequestItemView)holder.itemView).bind(pullRequests.get(position));
    }

    @Override
    public int getItemCount() {
        if (pullRequests == null)
            return 0;

        return pullRequests.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PullRequest pullRequest = pullRequests.get(getAdapterPosition());
                    Uri uri = Uri.parse(pullRequest.getHtmlUrl());
                    view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, uri));
                }
            });
        }
    }
}
