package com.doug.desafioandroid.ui.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.doug.desafioandroid.rest.datasource.PullRequestDataSource;
import com.doug.desafioandroid.rest.model.PullRequest;

import java.util.List;

/**
 * Created by doug on 19/10/17.
 */

public class PullRequestViewModel extends ViewModel {

    public MutableLiveData<List<PullRequest>> pullRequests;

    public LiveData<List<PullRequest>> getPullRequests(String ownerLogin, String repositoryName) {
        if (pullRequests == null) {
            pullRequests = new MutableLiveData<>();
            new PullRequestDataSource().loadPullRequests(ownerLogin, repositoryName, pullRequests);
        }
        return pullRequests;
    }

}
