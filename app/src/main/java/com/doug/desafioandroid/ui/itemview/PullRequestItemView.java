package com.doug.desafioandroid.ui.itemview;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.doug.desafioandroid.R;
import com.doug.desafioandroid.rest.model.PullRequest;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;

/**
 * Created by doug on 19/10/17.
 */

@EViewGroup(R.layout.pullrequest_item_view)
public class PullRequestItemView extends BaseItemView {

    @ViewById
    TextView pullRequestTitle, pullRequestBody, pullRequestCreatedDate, ownerLogin;

    @ViewById
    ImageView ownerAvatar;

    public PullRequestItemView(Context context) {
        super(context);
    }

    public void bind(PullRequest pullRequest) {
        pullRequestTitle.setText(pullRequest.getTitle());
        pullRequestBody.setText(pullRequest.getBody());
        ownerLogin.setText(pullRequest.getOwner().getLogin());

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        pullRequestCreatedDate.setText(sdf.format(pullRequest.getCreatedAt()));

        loadImage(ownerAvatar, pullRequest.getOwner().getAvatarURL());
    }

}
