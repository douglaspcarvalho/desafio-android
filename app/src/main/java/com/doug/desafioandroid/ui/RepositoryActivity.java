package com.doug.desafioandroid.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.doug.desafioandroid.R;
import com.doug.desafioandroid.ui.adapter.RepositoryAdapter;
import com.doug.desafioandroid.ui.viewmodel.RepositoryViewModel;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_repository)
public class RepositoryActivity extends BaseActivity {

    @ViewById
    RecyclerView repositoryRecyclerView;

    @Bean
    RepositoryAdapter repositoryAdapter;

    @AfterViews
    void afterViews(){
        repositoryRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        RepositoryViewModel viewModel = ViewModelProviders.of(this).get(RepositoryViewModel.class);
        viewModel.init();

        viewModel.repositories.observe(this, pagedList -> {
            repositoryAdapter.setList(pagedList);
            hideLoading();
        });

        repositoryRecyclerView.setAdapter(repositoryAdapter);

        showLoading();
    }

    //Add ScrollListener to know when list reached the bottom and show progress bar
    @AfterViews
    void addScrollListenerForProgressBar(){
        repositoryRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int totalItemCount = repositoryRecyclerView.getLayoutManager().getItemCount();
                int lastVisibleItem =
                        ((LinearLayoutManager)repositoryRecyclerView.getLayoutManager()).findLastVisibleItemPosition();


                if (totalItemCount <= (lastVisibleItem + 5)) {
                    showBottomLoading();
                }
            }
        });
    }

    //Add AdapterDataObserver to know when data is loaded to hide bottom progress bar
    @AfterInject
    void registerAdapterObserver(){
        RecyclerView.AdapterDataObserver observer= new RecyclerView.AdapterDataObserver() {

            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                hideBottomLoading();
            }
        };

        repositoryAdapter.registerAdapterDataObserver(observer);
    }

}
