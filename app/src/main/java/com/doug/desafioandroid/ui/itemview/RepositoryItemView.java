package com.doug.desafioandroid.ui.itemview;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.doug.desafioandroid.R;
import com.doug.desafioandroid.rest.model.Repository;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by doug on 19/10/17.
 */

@EViewGroup(R.layout.repository_item_view)
public class RepositoryItemView extends BaseItemView {

    @ViewById
    TextView repositoryName;

    @ViewById
    TextView repositoryDescription;

    @ViewById
    TextView forksCount;

    @ViewById
    TextView starsCount;

    @ViewById
    TextView ownerLogin;

    @ViewById
    ImageView ownerAvatar;

    public RepositoryItemView(Context context) {
        super(context);
    }

    public void bind(Repository repository){
        repositoryName.setText(repository.getName());
        repositoryDescription.setText(repository.getDescription());
        forksCount.setText(repository.getForksCount().toString());
        starsCount.setText(repository.getStarsCount().toString());
        ownerLogin.setText(repository.getOwner().getLogin());

        loadImage(ownerAvatar, repository.getOwner().getAvatarURL());
    }
}
