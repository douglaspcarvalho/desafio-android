package com.doug.desafioandroid.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;

import com.doug.desafioandroid.R;
import com.doug.desafioandroid.ui.adapter.PullRequestAdapter;
import com.doug.desafioandroid.ui.viewmodel.PullRequestViewModel;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

/**
 * Created by doug on 19/10/17.
 */

@EActivity(R.layout.activity_pullrequest)
public class PullRequestActivity extends BaseActivity {

    @ViewById
    RecyclerView pullRequestRecyclerView;

    @Extra
    String ownerLogin, repositoryName;

    @Bean
    PullRequestAdapter pullRequestAdapter;

    @AfterViews
    void afterViews(){
        pullRequestRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        PullRequestViewModel viewModel = ViewModelProviders.of(this).get(PullRequestViewModel.class);
        viewModel.getPullRequests(ownerLogin, repositoryName);

        viewModel.pullRequests.observe(this, pullRequests -> {
            pullRequestAdapter.setPullRequests(pullRequests);
            pullRequestAdapter.notifyDataSetChanged();
            hideLoading();
        });

        pullRequestRecyclerView.setAdapter(pullRequestAdapter);

        getSupportActionBar().setTitle(repositoryName);
        getSupportActionBar().setHomeButtonEnabled(true);

        showLoading();
    }

}
