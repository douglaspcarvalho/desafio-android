package com.doug.desafioandroid.ui.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.DataSource;
import android.arch.paging.LivePagedListProvider;
import android.arch.paging.PagedList;

import com.doug.desafioandroid.rest.datasource.RepositoryDataSource;
import com.doug.desafioandroid.rest.model.Repository;

/**
 * Created by doug on 18/10/17.
 */
public class RepositoryViewModel extends ViewModel {

    public LiveData<PagedList<Repository>> repositories;

    public void init(){
        if (repositories == null) {
            repositories = new LivePagedListProvider<Integer, Repository>(){
                @Override
                protected DataSource<Integer, Repository> createDataSource() {
                    return new RepositoryDataSource();
                }
            }.create(0, new PagedList.Config.Builder()
                    .setEnablePlaceholders(false)
                    .setPageSize(30)
                    .setPrefetchDistance(5)
                    .build());
        }
    }
}
