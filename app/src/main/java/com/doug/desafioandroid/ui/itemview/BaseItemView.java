package com.doug.desafioandroid.ui.itemview;

import android.content.Context;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.doug.desafioandroid.R;

/**
 * Created by doug on 20/10/17.
 */

public abstract class BaseItemView extends RelativeLayout{

    public BaseItemView(Context context) {
        super(context);
    }

    protected void loadImage(ImageView imageView, String url){
        RequestOptions options =
                new RequestOptions().placeholder(R.drawable.avatar_placeholder)
                        .transforms(new RoundedCorners(70));

        Glide.with(getContext())
                .load(url)
                .apply(options)
                .into(imageView);
    }
}
