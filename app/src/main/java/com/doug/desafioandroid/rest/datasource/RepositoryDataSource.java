package com.doug.desafioandroid.rest.datasource;

import android.arch.paging.DataSource;
import android.arch.paging.TiledDataSource;
import android.util.Log;

import com.doug.desafioandroid.rest.GitHubApi_;
import com.doug.desafioandroid.rest.model.Repository;
import com.doug.desafioandroid.rest.model.transport.RepositoryResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by doug on 19/10/17.
 */
public class RepositoryDataSource extends TiledDataSource<Repository> {

    private static final String LOG_TAG = "REPOSITORIES API CALL";

    @Override
    public int countItems() {
        return DataSource.COUNT_UNDEFINED;
    }

    @Override
    public List<Repository> loadRange(int startPosition, int count) {
        List<Repository> repositories = new ArrayList<>();

        do {
            repositories = executeCall(getPage(startPosition, count));
        }while (repositories.isEmpty());

        return repositories;
    }

    private List<Repository> executeCall(Integer page){
        List<Repository> repositories = new ArrayList<>();

        try {
            Response<RepositoryResponse> response =
                    GitHubApi_.getInstance_(null).getGitHubService().getRepositoryList(page).execute();
            if (response.isSuccessful()) {
                repositories.addAll(response.body().getRepositories());
            } else {
                Log.e(LOG_TAG, response.message());
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, e.getMessage());
        }

        if (repositories.isEmpty())
            sleepFiveSeconds();

        return repositories;
    }

    /**
     * if there's a network problem, sleep for 5 seconds after another try.
     */
    private void sleepFiveSeconds(){
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            Log.e(LOG_TAG, e.getMessage());
        }
    }

    /**
     * @return page number to load from api
     */
    private int getPage(int startPosition, int count){
        if (startPosition == 0)
            return 1;
        else
            return (startPosition/count) + 1;
    }

}
