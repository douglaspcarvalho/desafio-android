package com.doug.desafioandroid.rest.model.transport;

import com.doug.desafioandroid.rest.model.Repository;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by doug on 18/10/17.
 */

public class RepositoryResponse {

    @SerializedName("items")
    private List<Repository> repositories;

    public List<Repository> getRepositories() {
        return repositories;
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
    }
}
