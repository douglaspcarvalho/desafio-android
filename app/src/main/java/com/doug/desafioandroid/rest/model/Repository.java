package com.doug.desafioandroid.rest.model;

import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.DiffCallback;

import com.google.gson.annotations.SerializedName;

/**
 * Created by doug on 18/10/17.
 */

public class Repository {

    private Long id;
    private String name;
    private String description;

    @SerializedName("forks")
    private Integer forksCount;

    @SerializedName("stargazers_count")
    private Integer starsCount;

    private Owner owner;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getForksCount() {
        return forksCount;
    }

    public void setForksCount(Integer forksCount) {
        this.forksCount = forksCount;
    }

    public Integer getStarsCount() {
        return starsCount;
    }

    public void setStarsCount(Integer starsCount) {
        this.starsCount = starsCount;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public static final DiffCallback<Repository> DIFF_CALLBACK = new DiffCallback<Repository>() {
        @Override
        public boolean areItemsTheSame(@NonNull Repository oldRepository, @NonNull Repository newRepository) {
            // User properties may have changed if reloaded from the DB, but ID is fixed
            return oldRepository.getId() == newRepository.getId();
        }
        @Override
        public boolean areContentsTheSame(@NonNull Repository oldRepository, @NonNull Repository newRepository) {
            // NOTE: if you use equals, your object must properly override Object#equals()
            // Incorrectly returning false here will result in too many animations.
            return oldRepository.equals(newRepository);
        }
    };
}
