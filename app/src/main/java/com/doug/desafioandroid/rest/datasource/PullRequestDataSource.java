package com.doug.desafioandroid.rest.datasource;

import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.doug.desafioandroid.rest.GitHubApi_;
import com.doug.desafioandroid.rest.model.PullRequest;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by doug on 20/10/17.
 */
public class PullRequestDataSource {

    private static final String LOG_TAG = "PULLREQUEST API CALL";

    public void loadPullRequests(String ownerLogin, String repositoryName, MutableLiveData<List<PullRequest>> pullRequests){
        GitHubApi_.getInstance_(null).getGitHubService()
                .getPullRequestList(ownerLogin, repositoryName).enqueue(new Callback<List<PullRequest>>() {

            @Override
            public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                if (response.isSuccessful()) {
                    pullRequests.postValue(response.body());
                } else {
                    Log.d(LOG_TAG, response.message());
                    retryConnection(ownerLogin, repositoryName, pullRequests);
                }
            }

            @Override
            public void onFailure(Call<List<PullRequest>> call, Throwable t) {
                Log.d(LOG_TAG, t.getMessage());
                retryConnection(ownerLogin, repositoryName, pullRequests);
            }
        });
    }

    private void retryConnection(String ownerLogin, String repositoryName, MutableLiveData<List<PullRequest>> pullRequests){
        sleepFiveSeconds();
        loadPullRequests(ownerLogin, repositoryName, pullRequests);
    }

    /**
     * if there's a network problem, sleep for 5 seconds after another try.
     */
    private void sleepFiveSeconds(){
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            Log.e(LOG_TAG, e.getMessage());
        }
    }

}
