package com.doug.desafioandroid.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by doug on 18/10/17.
 */

public class Owner {

    private String login;

    @SerializedName("avatar_url")
    private String avatarURL;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public void setAvatarURL(String avatarURL) {
        this.avatarURL = avatarURL;
    }
}
