package com.doug.desafioandroid.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by doug on 19/10/17.
 */

public class PullRequest {

    private String title;
    private String body;

    @SerializedName("html_url")
    String htmlUrl;

    @SerializedName("created_at")
    private Date createdAt;

    @SerializedName("user")
    private Owner owner;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }
}
