package com.doug.desafioandroid.rest;

import com.doug.desafioandroid.rest.model.PullRequest;
import com.doug.desafioandroid.rest.model.transport.RepositoryResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by doug on 18/10/17.
 */

public interface GitHubService {

    @GET("search/repositories?q=language:Java&sort=stars")
    Call<RepositoryResponse> getRepositoryList(@Query("page") Integer page);

    @GET("repos/{owner}/{repo}/pulls")
    Call<List<PullRequest>> getPullRequestList(@Path("owner") String owner, @Path("repo") String repo);

}
