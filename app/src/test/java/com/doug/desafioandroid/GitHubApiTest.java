package com.doug.desafioandroid;

import com.doug.desafioandroid.rest.GitHubApi;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;


/**
 * Created by doug on 20/10/17.
 */

public class GitHubApiTest {

    @Test
    public void testGitHubServiceCreation(){
        GitHubApi api = new GitHubApi();

        assertNull(api.getGitHubService());

        api.config();

        assertNotNull(api.getGitHubService());
    }

}
