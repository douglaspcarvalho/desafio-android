package com.doug.desafioandroid;

import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.doug.desafioandroid.idlingresource.ProgressIdlingResource;
import com.doug.desafioandroid.ui.RepositoryActivity_;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.core.IsNot.not;

/**
 * Created by doug on 20/10/17.
 */

@RunWith(AndroidJUnit4.class)
public class RepositoryActivityInstrumentedTest  {

    @Rule
    public ActivityTestRule<RepositoryActivity_>
            mActivityRule = new ActivityTestRule<>(RepositoryActivity_.class);

    private IdlingResource mIdlingResource;
    private RepositoryActivity_ repositoryActivity_;

    @Before
    public void registerIdlingResource() {
        repositoryActivity_ = mActivityRule.getActivity();
    }

    @Test
    public void validateInitialLoadingIsShowing(){
        onView(withId(R.id.loading)).check(matches(isDisplayed()));
    }

    @Test
    public void openPullRequestOfAnItem() throws InterruptedException {
        ProgressIdlingResource progressIdlingResource = new ProgressIdlingResource(repositoryActivity_);
        IdlingRegistry.getInstance().register(progressIdlingResource);

        onView(withId(R.id.repositoryRecyclerView))
                .perform(RecyclerViewActions.scrollToPosition(27));

        onView(withId(R.id.loading)).check(matches(not(isDisplayed())));

        onView(withId(R.id.bottomLoading)).check(matches(isDisplayed()));

        onView(withId(R.id.repositoryRecyclerView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(27, click()));

        onView(withId(R.id.pullRequestRecyclerView)).check(matches(isDisplayed()));

        IdlingRegistry.getInstance().unregister(progressIdlingResource);
    }



}
