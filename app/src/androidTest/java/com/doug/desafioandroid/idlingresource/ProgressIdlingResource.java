package com.doug.desafioandroid.idlingresource;

import android.support.test.espresso.IdlingResource;

import com.doug.desafioandroid.ui.BaseActivity;

/**
 * Created by doug on 20/10/17.
 */

public class ProgressIdlingResource implements IdlingResource {

    private IdlingResource.ResourceCallback resourceCallback;
    private BaseActivity baseActivity;
    private BaseActivity.ProgressListener progressListener;

    public ProgressIdlingResource(BaseActivity activity){
        baseActivity = activity;

        progressListener = new BaseActivity.ProgressListener() {
            @Override
            public void onProgressShown() {
            }
            @Override
            public void onProgressDismissed() {
                if (resourceCallback == null){
                    return ;
                }
                //Called when the resource goes from busy to idle.
                resourceCallback.onTransitionToIdle();
            }
        };

        baseActivity.setProgressListener (progressListener);
    }
    @Override
    public String getName() {
        return "My idling resource";
    }

    @Override
    public boolean isIdleNow() {
        // the resource becomes idle when the progress has been dismissed
        return !baseActivity.isLoadingShowing();
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback resourceCallback) {
        this.resourceCallback = resourceCallback;
    }
}

