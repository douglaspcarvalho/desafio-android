package com.doug.desafioandroid;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.doug.desafioandroid.idlingresource.ProgressIdlingResource;
import com.doug.desafioandroid.ui.PullRequestActivity_;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.core.IsNot.not;

/**
 * Created by doug on 20/10/17.
 */

@RunWith(AndroidJUnit4.class)
public class PullRequestInstrumentedTest {

    @Rule
    public IntentsTestRule<PullRequestActivity_>
            mActivityRule = new IntentsTestRule<PullRequestActivity_>(
                    PullRequestActivity_.class, true, false);

    private IdlingResource mIdlingResource;
    private PullRequestActivity_ pullRequestActivity_;

    @Before
    public void registerIdlingResource() {
    }

    @Before
    public void startActivity(){
        Context targetContext = InstrumentationRegistry.getInstrumentation()
                .getTargetContext();
        Intent intent = new Intent(targetContext, PullRequestActivity_.class);
        intent.putExtra("ownerLogin", "elastic");
        intent.putExtra("repositoryName", "elasticsearch");

        mActivityRule.launchActivity(intent);

        pullRequestActivity_ = mActivityRule.getActivity();
    }

    @Test
    public void validateInitialLoadingIsShowing(){
        onView(withId(R.id.loading)).check(matches(isDisplayed()));
    }

    @Test
    public void openPullRequestOnBrowserForAnItem(){
        ProgressIdlingResource progressIdlingResource = new ProgressIdlingResource(pullRequestActivity_);
        IdlingRegistry.getInstance().register(progressIdlingResource);

        onView(withId(R.id.loading)).check(matches(not(isDisplayed())));

        onView(withId(R.id.pullRequestRecyclerView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(2, click()));

        //To Work, a default browser must be setted.
        intended(hasAction(Intent.ACTION_VIEW));

        IdlingRegistry.getInstance().unregister(progressIdlingResource);
    }

}
